package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "tests")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private float minTemperature;
    private float maxTemperature;
    private float avrTemperature;
    private float minHumidity;
    private float maxHumidity;
    private float avrHumidity;
    private float minPressure;
    private float maxPressure;
    private float avrPressure;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(float minTemperature) {
        this.minTemperature = minTemperature;
    }

    public float getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(float maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public float getAvrTemperature() {
        return avrTemperature;
    }

    public void setAvrTemperature(float avrTemperature) {
        this.avrTemperature = avrTemperature;
    }

    public float getMinHumidity() {
        return minHumidity;
    }

    public void setMinHumidity(float minHumidity) {
        this.minHumidity = minHumidity;
    }

    public float getMaxHumidity() {
        return maxHumidity;
    }

    public void setMaxHumidity(float maxHumidity) {
        this.maxHumidity = maxHumidity;
    }

    public float getAvrHumidity() {
        return avrHumidity;
    }

    public void setAvrHumidity(float avrHumidity) {
        this.avrHumidity = avrHumidity;
    }

    public float getMinPressure() {
        return minPressure;
    }

    public void setMinPressure(float minPressure) {
        this.minPressure = minPressure;
    }

    public float getMaxPressure() {
        return maxPressure;
    }

    public void setMaxPressure(float maxPressure) {
        this.maxPressure = maxPressure;
    }

    public float getAvrPressure() {
        return avrPressure;
    }

    public void setAvrPressure(float avrPressure) {
        this.avrPressure = avrPressure;
    }

    public Test() {

    }

}
