package com.example.demo.models;



import javax.persistence.*;
import javax.validation.constraints.NotNull;
import  java.util.*;

@Entity
@Table(name = "devices")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Column(nullable = false)
    private String name;
    private String datasheet;


    @OneToMany(cascade = CascadeType.ALL)
    private List<Test> tests = new ArrayList<>();

    public Device(String name){
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatasheet() {
        return datasheet;
    }

    public void setDatasheet(String datasheet) {
        this.datasheet = datasheet;
    }
}
